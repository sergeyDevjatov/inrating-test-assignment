import * as LocalStorage from '@/localStorage';


const state = {
  users: [],
};

const mutations = {
  getUsers (state) {
    state.users = LocalStorage.getUsers();
  },
  addUser(state, { user }) {
    const result = LocalStorage.addUser(user);
    state.users = [...state.users, result];
  },
  updateUser(state, { id, user: nextUser }) {
    const result = LocalStorage.updateUser(id, nextUser);
    state.users = state.users.map((user) => `${user.id}` === `${id}`
     ? result
     : user).sort((left, right) => left.id - right.id);
  },
  deleteUser(state, { id }) {
    LocalStorage.deleteUser(id);
    state.users = state.users.filter((user) => `${user.id}` !== `${id}`);
  },
};

export default {
  namespaced: true,
  state,
  mutations
}
