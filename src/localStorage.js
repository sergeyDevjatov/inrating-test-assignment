export function getUsers() {
  let users = JSON.parse(localStorage.getItem('users'));
  if(!users) {
    users = [];
    setUsers(users);
  }
  return users.filter(user => user) ;
}

function setUsers(users) {
  localStorage.setItem('users', JSON.stringify(users));
}

function getLastId() {
  let lastId = JSON.parse(localStorage.getItem('users-last-id'));
  if(!lastId) {
    lastId = 0;
    setLastId(lastId);
  }
  return lastId;
}

function setLastId(id) {
  localStorage.setItem('users-last-id', `${id}`);
}

export function addUser(user) {
  const users = getUsers() || [],
    nextId = getLastId() + 1,
    userWithId = {id: nextId, ...user};
  users.push(userWithId);
  setUsers(users);
  setLastId(nextId);
  return userWithId;
}

export function updateUser(id, nextUser) {
  const users = getUsers(),
    nextUserWithId = {id, ...nextUser};

  setUsers(users.map(
    user => `${user.id}` === `${id}`
      ? nextUserWithId
      : user
  ));
  return nextUserWithId;
}

export function deleteUser(id) {
  const users = getUsers();
  setUsers(users.filter(
    user => `${user.id}` !== `${id}`
  ));
}
