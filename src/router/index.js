import Vue from 'vue';
import Router from 'vue-router';
import UserList from '@/components/UserList';
import UserEdit from '@/components/Useredit';


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'UserList',
      component: UserList,
    },
    {
      path: '/create',
      name: 'UserCreate',
      component: UserEdit,
    },
    {
      path: '/:userId',
      name: 'UserUpdate',
      component: UserEdit,
      props: true
    },
  ]
});
